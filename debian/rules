#!/usr/bin/make -f
# GNU copyright 2004 by Mattia Dongili.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1


# These are used for cross-compiling and for saving the configure script
# from having to guess our platform (since we know it already)
DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)


CFLAGS = -Wall -g

ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -O0
else
	CFLAGS += -O2
endif
ifeq (,$(findstring nostrip,$(DEB_BUILD_OPTIONS)))
	INSTALL_PROGRAM += -s
endif

config-stamp:
	dh_testdir
	autoreconf -vfi
	# configure the package.
	./configure --host=$(DEB_HOST_GNU_TYPE) --build=$(DEB_BUILD_GNU_TYPE) --prefix=/usr --mandir=\$${prefix}/share/man --infodir=\$${prefix}/share/info --sysconfdir=/etc --localstatedir=/var/run --libdir=/usr/lib/cpufreqd
	touch config-stamp

build: build-stamp
build-stamp:  config-stamp
	dh_testdir
	# compile the package.
	$(MAKE)
	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp 

	# clean up after the build process.
	[ ! -f Makefile ] || $(MAKE) distclean
	dh_clean
#	debconf-updatepo

install: build
	dh_testdir
	dh_testroot
	dh_clean
	dh_installdirs

	# install the package into debian/cpufreqd.
	$(MAKE) install DESTDIR=$(CURDIR)/debian/cpufreqd
	install -m 0644 debian/cpufreqd.conf $(CURDIR)/debian/cpufreqd/etc/


# Build architecture-independent files here.
binary-indep: build install
# We have nothing to do by default.

# Build architecture-dependent files here.
binary-arch: build install
	dh_testdir
	dh_testroot
	dh_installchangelogs ChangeLog
	dh_installdocs
	dh_installexamples
#	dh_installdebconf	
	dh_installinit -n
	dh_installman
	dh_link
	dh_strip
	dh_compress
	dh_fixperms
	dh_makeshlibs
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install 
